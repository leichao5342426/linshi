//第一种方法定义sock2网络环境
//必须写在文件的最前面
//如果工程量大的话 就不能保证Winsock2.h是不是在前面,因此还有第二种写法
#define WIN32_LEAN_AND_MEAN
#include<WinSock2.h>
#include<windows.h>
#pragma comment(lib,"ws2_32.lib")
int main() {
	//版本号
	WORD ver = MAKEWORD(2,2);
	WSADATA dat;
	WSAStartup(ver, &dat);
	//----------------
	//--用socketAPl建立简易tcp 客户端
	//1.建立一个socket
	//2.连接服务器 connect
	//3.接受服务器信息recv
	//4.关闭套子节closescoket
	WSACleanup();
	return 0;
}