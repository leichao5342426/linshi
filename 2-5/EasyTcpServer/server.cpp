//第一种方法定义sock2网络环境
//必须写在文件的最前面
//如果工程量大的话 就不能保证Winsock2.h是不是在前面,因此还有第二种写法
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include<WinSock2.h>
#include<windows.h>
#include <stdio.h>

#pragma comment(lib,"ws2_32.lib")

int main() {
	//版本号
	WORD ver = MAKEWORD(2, 2);
	WSADATA dat;
	WSAStartup(ver, &dat);

	//--用socketAPl建立简易tcp 服务器
	//1.建立一个socket 套接字
	SOCKET _sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	//2.bind绑定用于接受客户端连接的网络端口
	sockaddr_in _sin = {};
	_sin.sin_family = AF_INET;
	_sin.sin_port = htons(4567);//host to net unsigned short
	_sin.sin_addr.S_un.S_addr = INADDR_ANY;//inet_addr("127.0.0.1");
	if (SOCKET_ERROR==bind(_sock, (sockaddr*)&_sin, sizeof(_sin))) {
		printf("ERROR,绑定用于接受客户端连接的网络端口失败");
	}
	else {
		printf("绑定网络端口成功 ....\n");
	
	}
	//3.listen 监听网络端口
	if (SOCKET_ERROR == listen(_sock, 5)) {
	
		printf("错误，监听网络端口失败");
	}
	else
	{
		printf("监听网路端口成功");
	}
	//4.accept 等待接受客户端连接
	sockaddr_in clientAddr = {};
	int nAddrLen = sizeof(sockaddr_in);
	SOCKET _cSock = INVALID_SOCKET;
	char msgBuf[] = "Hello ,I'm server";
	while(true)
	{
	_cSock=accept(_sock,(sockaddr*)&clientAddr,&nAddrLen);
	if (INVALID_SOCKET == _cSock)
	{
		printf("错误，接收到客户端socket ......");
	
	}
	printf("新客户端加入: IP= %s \n", inet_ntoa(clientAddr.sin_addr));
	//5.send 向客户端发送一条数据
	
	send(_cSock, msgBuf, strlen(msgBuf) + 1, 0);
	}
	//6.关闭套子节closesocket
	closesocket(_sock);
	//-------
	//清除windows  socket环境

	WSACleanup();
	return 0;
}


