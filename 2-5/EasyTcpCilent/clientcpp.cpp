//第一种方法定义sock2网络环境
//必须写在文件的最前面
//如果工程量大的话 就不能保证Winsock2.h是不是在前面,因此还有第二种写法

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include<WinSock2.h>
#include<windows.h>
#include <stdio.h>
#pragma comment(lib,"ws2_32.lib")
int main() {
	//版本号
	WORD ver = MAKEWORD(2, 2);
	WSADATA dat;
	WSAStartup(ver, &dat);
	//----------------
	//--用socketAPl建立简易tcp 客户端
	//1.建立一个socket
	SOCKET _sock=	socket(AF_INET,SOCK_STREAM,0);
	if (INVALID_SOCKET == _sock) {
		printf("错误，建立socket失败");

	
	
	}
	else 
	{
		printf("建立socket成功");

	}

	//2.连接服务器 connect
	sockaddr_in _sin =  {};
	_sin.sin_family = AF_INET;
	_sin.sin_port = htons(4567);
	_sin.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	;
	int  ret = connect(_sock, (sockaddr*)&_sin, sizeof(sockaddr_in));
	if (SOCKET_ERROR == ret) {
		printf("错误，建立socket失败");



	}
	else
	{
		printf("建立socket成功");

	}


	//3.接受服务器信息recv
	char recvBuf[256] = {};
int nlen=recv(_sock, recvBuf, 256,0);
if (nlen > 0)
{
	printf("接收数据:%s \n", recvBuf);


}
	//4.关闭套子节closescoket
	closesocket(_sock);

	//清除Windows socket 环境

	WSACleanup();
	getchar();
	return 0;
}